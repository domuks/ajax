def index():
    return dict()

def data():
    if not session.m or len(session.m)==10: session.m=[]
    if request.vars.kiek:
        #         rez2 = int(request.vars.kiek) * 2
        kiek = int(request.vars.kiek)
        session.spejimai.append(kiek)
        if kiek == session.tikslas:
            rez2 = "valio"
        elif kiek > session.tikslas:
            rez2 = "per daug"
        elif kiek < session.tikslas:
            rez2 = "per mažai"
    return DIV(BEAUTIFY(request.vars), rez2, UL(session.spejimai))

def start():
    from random import randint
    session.tikslas = randint(1, 10)
    session.spejimai = []
    return A("eik speliot..",_href="index")

def bum():
    rez = FORM("kiek:",INPUT(_name="kiek"),BUTTON("siusti",_type="submit"))
    rez2 = ""
    if request.vars.kiek:
#         rez2 = int(request.vars.kiek) * 2
             kiek = int(request.vars.kiek)
             session.spejimai.append(kiek)
             if kiek == session.tikslas:
                 rez2 = "valio"
             elif kiek > session.tikslas:
                 rez2 = "per daug"
             elif kiek < session.tikslas:
                 rez2 = "per mažai"
    return DIV(rez, BEAUTIFY(request.vars),rez2,UL(session.spejimai))

def reset():
    session.clear()
    redirect('notes')